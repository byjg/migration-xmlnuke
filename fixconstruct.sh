#!/bin/bash

FILES=`find . -name '*.class.php'`;

for FILE in $FILES
do
	CLASS=`basename "$FILE" | cut -d\. -f1`
	if grep --quiet "function $CLASS" $FILE 
	then
		echo "$FILE: $CLASS: sed -e 's/function $CLASS/function __construct/g"
	fi
done;
