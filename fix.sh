#!/bin/bash

SELFPATH=`dirname $0`
RULES=`cat $SELFPATH/rules.txt`
SEARCHPATH=$1

if [ -z "$1" ]
then
	echo "ERROR: fix PATH [ruleno] "
	echo
	exit
fi

line=0

for rule in $RULES
do

	line=$((line+1))

	if [ ! -z "$2" ] && [ "$2" -ne "$line" ]
	then
		continue;
	fi

	echo "Rule $line: $rule"
	find $SEARCHPATH -type f -name '*.php' -exec sed -i "$rule" {} \; -print
	echo

done
