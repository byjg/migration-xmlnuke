#!/bin/bash

FILES=`find . -name '*.class.php'`;

for FILE in $FILES
do
	NEWNAME=`grep "class " $FILE | cut -d\  -f2`
	echo "git mv $FILE `dirname $FILE`/$NEWNAME.php";
done;
